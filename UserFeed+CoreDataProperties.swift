//
//  UserFeed+CoreDataProperties.swift
//  SBNRI App
//
//  Created by Nitin Singh on 11/04/20.
//
//

import Foundation
import CoreData


extension UserFeed {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserFeed> {
        return NSFetchRequest<UserFeed>(entityName: "UserFeed")
    }

    @NSManaged public var name: String?
    @NSManaged public var desc: String?
    @NSManaged public var openIssues: Int16
    @NSManaged public var license: String?
    @NSManaged public var permissions: Bool

}
