//
//  WebClient.swift
//  SBNRI App
//
//  Created by Nivesh on 09/04/20.
//

import Foundation
public typealias JSON = [String: Any]
public typealias HTTPHeaders = [String: String]

public enum RequestMethod: String {
  case get = "GET"
  case post = "POST"
  case put = "PUT"
  case delete = "DELETE"
}

extension URL {
  init<A, E>(baseUrl: String, resource: Resource<A, E>) {
    var components = URLComponents(string: baseUrl)!
    let resourceComponents = URLComponents(string: resource.path.absolutePath)!
    
    components.path = Path(components.path).appending(path: Path(resourceComponents.path)).absolutePath
    components.queryItems = resourceComponents.queryItems
    
    switch resource.method {
    case .get, .delete:
      var queryItems = components.queryItems ?? []
      queryItems.append(contentsOf: resource.params.map {
        URLQueryItem(name: $0.key, value: String(describing: $0.value))
      })
      components.queryItems = queryItems
    default:
      break
    }
    
    self = components.url!
  }
}

extension URLRequest {
  init<A, E>(baseUrl: String, resource: Resource<A, E>) {
    let url = URL(baseUrl: baseUrl, resource: resource)
    self.init(url: url)
    httpMethod = resource.method.rawValue
    resource.headers.forEach {
      setValue($0.value, forHTTPHeaderField: $0.key)
    }
    switch resource.method {
    case .post, .put:
      httpBody = try! JSONSerialization.data(withJSONObject: resource.params, options: [])
    default:
      break
    }
  }
}

open class WebClient {
  private var baseUrl: String
  
  public var commonParams: JSON = [:]
  
  init() {
    self.baseUrl = "https:/api.github.com"
  }
  
  public func load<A, CustomError>(resource: Resource<A, CustomError>,completion: @escaping (Result<A, CustomError>) -> Void) -> URLSessionDataTask? {
    
    if(!AppUtils.isInternetConnectivityAvailable(true)) {
      completion(.failure(.noInternetConnection))
      return nil
    }
    
    var newResouce = resource
    newResouce.params = newResouce.params.merging(commonParams) { spec, _ in
      return spec
    }
    
    let request = URLRequest(baseUrl: baseUrl, resource: newResouce)
    
    print(request, newResouce.params)
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      // Parsing incoming data
      guard let response = response as? HTTPURLResponse else {
        debugPrint(error?.localizedDescription as Any)
        completion(.failure(.other))
        return
      }
      
      if (200..<300) ~= response.statusCode {
        self.printReceivedResponse(data!)
        completion(Result(value: data.flatMap(resource.parse), or: .other))
      } else if response.statusCode == 401 {
        completion(.failure(.unauthorized))
      } else {
        completion(.failure(data.flatMap(resource.parseError).map({.custom($0)}) ?? .other))
      }
    }
    
    task.resume()
    
    return task
    
  }
  
  
  func printReceivedResponse(_ data: Data) {
    let parsedData : Any?
    do {
      parsedData =  try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
    } catch {
      parsedData = nil
    }
    
    let dataAsString : NSString? = NSString(data: data,encoding: String.Encoding.utf8.rawValue)
    
    if dataAsString != nil {
      print("\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(dataAsString!)\n\n\n")
    }
    
    if parsedData != nil {
      print("\n\nRECEIVED DATA AFTER PARSING IS \n\n\(parsedData!)\n\n\n")
    }
  }
}
