//
//  WebError.swift
//  SBNRI App
//
//  Created by Nivesh on 09/04/20.
//

import Foundation

public enum WebError<CustomError>: Error {
    case noInternetConnection
    case custom(CustomError)
    case unauthorized
    case other
}

// MARK:- Handle Custom error
struct CustomError: Error, Decodable {
    var message: String
}
