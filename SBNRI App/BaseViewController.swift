//
//  BaseViewController.swift
//  SBNRI App
//
//  Created by Nivesh on 09/04/20.
//

import UIKit

var vSpinner : UIView?

class BaseViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        
        return refreshControl
    }()
    
    
    static let client = WebClient()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTableView()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
    }
    
    func setUpTableView() {
        self.tableView.separatorStyle = .singleLine
        self.tableView.backgroundColor = UIColor.white
        self.tableView.estimatedRowHeight = 250
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.hideExtraRowsFromTable()
        self.tableView.showsVerticalScrollIndicator = false
        self.registerCellWithTableView()
    }
    
    func registerCellWithTableView() {
        self.tableView.registerTableViewWithCell(nibName: "TableViewCell")
    }
    
    // MARK: - handle Errors for APIs
    func handleError(_ error: WebError<CustomError>) {
        switch error {
        case .noInternetConnection:
            print("The internet connection is lost")
            self.removeSpinner()
        case .unauthorized:
            print("Unauthrized")
        case .other:
            print("Something went wrong")
        case .custom(let error):
            print(error.message)
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
    }
    
}

// MARK:- Check for internet connection
class AppUtils: NSObject {
    class func isInternetConnectivityAvailable (_ show:Bool) -> Bool {
        var isReachable : Bool = true
        if let networkReachability = Reachability() {
            let networkStatus = networkReachability.currentReachabilityStatus
            isReachable = (networkStatus != Reachability.NetworkStatus.notReachable)
        }
        if show && !isReachable {
            print("No internet!")
        }
        return isReachable
    }
}

//MARK:- Create an extension of tableview for registering cells and hide extra rows
extension UITableView {
    public func registerTableViewWithCell(nibName:String) {
        self.register(UINib(nibName: nibName as String, bundle: nil), forCellReuseIdentifier: nibName as String)
    }
    
    public func hideExtraRowsFromTable() {
        self.sectionFooterHeight = 0.0
        self.tableFooterView = UIView(frame: CGRect.zero)
        self.sectionHeaderHeight = 0.0
    }
}

//MARK:- Create a activity indicator
extension UIViewController {
    func showSpinner(onView : UIView) {
        if vSpinner == nil {
            let spinnerView = UIView.init(frame: onView.bounds)
            spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            let activityIndicator = UIActivityIndicatorView.init(style: .large)
            activityIndicator.startAnimating()
            activityIndicator.center = spinnerView.center
            
            DispatchQueue.main.async {
                spinnerView.addSubview(activityIndicator)
                onView.addSubview(spinnerView)
            }
            vSpinner = spinnerView
        }
    }
    
    func removeSpinner() {
        if vSpinner != nil {
            DispatchQueue.main.async {
                vSpinner?.removeFromSuperview()
                vSpinner = nil
            }
        }
    }
}
