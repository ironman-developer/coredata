//
//  ViewController.swift
//  SBNRI App
//
//  Created by Nivesh on 09/04/20.
//

import UIKit
import CoreData

class ViewController: BaseViewController {
    
    var sessionTask: URLSessionDataTask?
    
    var page:Int = 1
    
    var page_Size: Int = 10
    
    var isLoading = false
    
    private let persistentContainer = NSPersistentContainer(name: "SBNRI_App")
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<UserFeed> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<UserFeed> = UserFeed.fetchRequest()
        
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(UserFeed.name), ascending: true)]
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.persistentContainer.viewContext, sectionNameKeyPath: #keyPath(UserFeed.name), cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        persistentContainer.loadPersistentStores { (persistentStoreDescription, error) in
            if let error = error {
                print("Unable to Load Persistent Store")
                print("\(error), \(error.localizedDescription)")
                
            } else {
                self.loadData()
                
                do {
                    try self.fetchedResultsController.performFetch()
                } catch {
                    let fetchError = error as NSError
                    print("Unable to Perform Fetch Request")
                    print("\(fetchError), \(fetchError.localizedDescription)")
                }
            }
        }
    }
    
    override func handleRefresh(_ refreshControl: UIRefreshControl) {
        super.handleRefresh(refreshControl)
        self.page = 1
        self.loadData()
    }
    
    func loadData() {
        self.getData()
        self.isLoading = false
    }
    
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            DispatchQueue.global().async {
                self.page = self.page + 1
                self.getData()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if (offsetY > contentHeight - scrollView.frame.height) && !isLoading {
            loadMoreData()
        }
    }
    
    //MARK: - Server call
    func getData() {
        sessionTask?.cancel()
        var params:[String: Any] = [String:Any]()
        params["page"] = page
        params["per_page"] = page_Size
        let requestUrl = "/orgs/octokit/repos"
        let resources = Resource<[DataModel], CustomError>(jsonDecoder: JSONDecoder(), path:requestUrl, method: .get, params: params)
        DispatchQueue.main.async {
            self.showSpinner(onView: self.view)
        }
        sessionTask = BaseViewController.client.load(resource: resources) {[weak self] result in
            guard let sself = self else { return }
            DispatchQueue.main.async {
               sself.removeSpinner()
                if let data  = result.value {
                    if sself.page == 1 {
                        sself.clearData()
                    }
                    if !data.isEmpty {
                        sself.isLoading = false
                        // saving data into core data
                        sself.saveInCoreDataWith(array: data)
                    } else {
                        // Do not request if server array becomes empty
                        sself.isLoading = true
                    }
                } else if let error = result.error {
                    sself.handleError(error)
                }
            }
        }
    }
    
    private func saveInCoreDataWith(array: [DataModel]) {
        _ = array.map{self.createPhotoEntityFrom(dictionary: $0)}
        do {
            try persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    //MARK:- Saving data to core data using codable models
    private func createPhotoEntityFrom(dictionary: DataModel) -> NSManagedObject? {
        let context = persistentContainer.viewContext
        if let feedEntity = NSEntityDescription.insertNewObject(forEntityName: "UserFeed", into: context) as? UserFeed {
            feedEntity.name = dictionary.name
            feedEntity.desc = dictionary.description
            feedEntity.license = dictionary.license?.name
            feedEntity.openIssues = dictionary.open_issues_count!
            if let permission = dictionary.permissions?.admin {
                feedEntity.permissions = permission
            }
            return feedEntity
        }
        return nil
    }
    
    // MARK:- Clear Core data if request page is 1
    private func clearData() {
        do {
            
            let context = persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: UserFeed.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

// MARK:- FetchedResultsController Delegates
extension ViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        tableView.reloadData()
    }
}

//MARK:- TableView Delegates and Datasources
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else {
            fatalError("Unexpected Index Path")
        }
        // Configure Cell
        configure(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func configure(_ cell: TableViewCell, at indexPath: IndexPath) {
        // Fetch UserFeed
        let userFeed = fetchedResultsController.object(at: indexPath)
        // Configure Cell
        cell.nameLbl.text = userFeed.name
        cell.descLbl.text = userFeed.desc
        cell.openIssuesLbl.text = "\(userFeed.openIssues)"
        cell.licenseNameLbl.text = userFeed.license
        cell.permissionsLbl.text = "\(userFeed.permissions)"
    }
    
}
