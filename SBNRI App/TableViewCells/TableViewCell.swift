//
//  TableViewCell.swift
//  SBNRI App
//
//  Created by Nivesh on 09/04/20.
//

import UIKit

class TableViewCell: UITableViewCell {
  
  @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var descLbl: UILabel!

    @IBOutlet weak var openIssuesLbl: UILabel!

    @IBOutlet weak var licenseNameLbl: UILabel!

    @IBOutlet weak var permissionsLbl: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
  }
    
}
