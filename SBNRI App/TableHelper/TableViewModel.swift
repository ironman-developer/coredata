//
//  TableViewModel.swift
//  SBNRI App
//
//  Created by Nivesh on 09/04/20.
//

import Foundation

struct DataModel : Codable {
  let name : String?
  let description : String?
  let open_issues_count : Int16?
  let license : License?
  let permissions : Permissions?
  
  
  enum CodingKeys: String, CodingKey {
    
    case name = "name"
    case description = "description"
    case open_issues_count = "open_issues_count"
    case license = "license"
    case permissions = "permissions"
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    name = try values.decodeIfPresent(String.self, forKey: .name)
    description = try values.decodeIfPresent(String.self, forKey: .description)
    open_issues_count = try values.decodeIfPresent(Int16.self, forKey: .open_issues_count)
    license = try values.decodeIfPresent(License.self, forKey: .license)
    permissions = try values.decodeIfPresent(Permissions.self, forKey: .permissions)
  }
  
}

struct License : Codable {
  let key : String?
  let name : String?
  let spdx_id : String?
  let url : String?
  let node_id : String?
  
  enum CodingKeys: String, CodingKey {
    // make these optionals
    case key
    case name
    case spdx_id
    case url
    case node_id
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    key = try values.decodeIfPresent(String.self, forKey: .key)
    name = try values.decodeIfPresent(String.self, forKey: .name)
    spdx_id = try values.decodeIfPresent(String.self, forKey: .spdx_id)
    url = try values.decodeIfPresent(String.self, forKey: .url)
    node_id = try values.decodeIfPresent(String.self, forKey: .node_id)
  }
  
}

struct Permissions : Codable {
  let admin : Bool?
  let push : Bool?
  let pull : Bool?
  
  enum CodingKeys: String, CodingKey {
    case admin
    case push
    case pull
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    admin = try values.decodeIfPresent(Bool.self, forKey: .admin)
    push = try values.decodeIfPresent(Bool.self, forKey: .push)
    pull = try values.decodeIfPresent(Bool.self, forKey: .pull)
  }
  
}
